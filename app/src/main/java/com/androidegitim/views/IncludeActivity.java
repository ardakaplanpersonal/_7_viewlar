package com.androidegitim.views;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import butterknife.OnClick;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class IncludeActivity extends BaseActivity {


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_include);
    }

    @OnClick(R.id.tus)
    public void click() {

        Log.d("APP", "TUSA BASILDI");
    }

}
