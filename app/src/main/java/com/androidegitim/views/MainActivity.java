package com.androidegitim.views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.androidegitim.views.myadapter.ListViewMyAdapter;
import com.androidegitim.views.recyclerview.RecyclerViewActivity;

import butterknife.OnClick;

public class MainActivity extends BaseActivity {


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_main);
    }

    @OnClick(R.id.main_button_size)
    public void size() {

        startActivity(new Intent(MainActivity.this, SizeActivity.class));
    }

    @OnClick(R.id.main_button_textview)
    public void textview() {

        startActivity(new Intent(MainActivity.this, TextviewActivity.class));
    }

    @OnClick(R.id.main_button_edittext)
    public void edittext() {

        startActivity(new Intent(MainActivity.this, EdittextActivity.class));
    }

    @OnClick(R.id.main_button_button)
    public void button() {

        startActivity(new Intent(MainActivity.this, ButtonActivity.class));
    }

    @OnClick(R.id.main_button_imageview)
    public void imageview() {

        startActivity(new Intent(MainActivity.this, ImageViewActivity.class));
    }

    @OnClick(R.id.main_button_scroll)
    public void scrollView() {

        startActivity(new Intent(MainActivity.this, ScrollActivity.class));
    }

    @OnClick(R.id.main_button_include)
    public void includeView() {

        startActivity(new Intent(MainActivity.this, IncludeActivity.class));
    }


    @OnClick(R.id.main_button_view_in_code)
    public void viewIncode() {

        startActivity(new Intent(MainActivity.this, ViewInCodeActivity.class));
    }

    @OnClick(R.id.main_button_inflate)
    public void inflate() {

        startActivity(new Intent(MainActivity.this, InflateActivity.class));
    }

    @OnClick(R.id.main_button_listview_array_adapter)
    public void listViewArrayAdapter() {

        startActivity(new Intent(MainActivity.this, ListViewArrayAdapter.class));
    }

    @OnClick(R.id.main_button_listview_my_adapter)
    public void listViewMyAdapter() {

        startActivity(new Intent(MainActivity.this, ListViewMyAdapter.class));
    }

    @OnClick(R.id.main_button_recycler_view)
    public void recyclerView() {

        startActivity(new Intent(MainActivity.this, RecyclerViewActivity.class));
    }
}
