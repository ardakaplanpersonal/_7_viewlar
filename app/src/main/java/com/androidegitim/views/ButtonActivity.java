package com.androidegitim.views;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import butterknife.OnClick;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ButtonActivity extends BaseActivity {

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_button);


        //buton tıklanmasına erişme 1
        Button button1 = findViewById(R.id.button1);

        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("VIEW", "TUŞ 1 tıklandı.");
            }
        });
    }


    @OnClick(R.id.button2)
    public void buton2() {
        Log.i("VIEW", "TUŞ 2 tıklandı.- butter knife");
    }
    

    public void buton3(View view) {
        Log.i("VIEW", "TUŞ 3 tıklandı.");
    }
}
