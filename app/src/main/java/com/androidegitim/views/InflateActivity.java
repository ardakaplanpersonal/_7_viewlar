package com.androidegitim.views;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class InflateActivity extends BaseActivity {

    @BindView(R.id.inflate_linerlayout_root)
    LinearLayout rootLinearLayout;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_inflate);

        View view = getLayoutInflater().inflate(R.layout.layout_button, null);

        rootLinearLayout.addView(view);

    }
}
