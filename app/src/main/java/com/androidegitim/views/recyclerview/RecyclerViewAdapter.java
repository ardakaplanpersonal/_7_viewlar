package com.androidegitim.views.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidegitim.views.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHoler> {

    private List<String> dataList;

    public RecyclerViewAdapter(List<String> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecyclerViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.layout_recycler_view, parent, false);

        return new RecyclerViewHoler(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHoler holder, int position) {

        holder.textView.setText(dataList.get(position));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class RecyclerViewHoler extends RecyclerView.ViewHolder {

        @BindView(R.id.recyler_textview)
        TextView textView;

        RecyclerViewHoler(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }

    }
}
