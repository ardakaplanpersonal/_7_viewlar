package com.androidegitim.views;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ViewInCodeActivity extends BaseActivity {

    @BindView(R.id.view_incode_linearlayout_root)
    LinearLayout rootLinearLayout;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_view_incode);

        addButton();

        addTextView();
    }

    private void addButton() {

        Button button = new Button(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        button.setLayoutParams(layoutParams);

        button.setText("KODLA YARATILAN TUŞ");

        rootLinearLayout.addView(button);
    }

    private void addTextView() {

        TextView textView = new TextView(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        textView.setLayoutParams(layoutParams);

        textView.setText("KODLA YARATILAN TEXTVIEW");

        textView.setGravity(Gravity.CENTER);

        textView.setTextColor(Color.RED);

        rootLinearLayout.addView(textView);
    }
}
