package com.androidegitim.views.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidegitim.views.R;

import java.util.List;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Adapter<String> extends BaseAdapter {

    private Activity activity;
    private List<String> dataList;

    public Adapter(Activity activity, List<String> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public String getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = LayoutInflater.from(activity).inflate(R.layout.row_layout_listview, parent, false);
        }

        String item = getItem(position);

        TextView textView = convertView.findViewById(R.id.row_layout_listview_textview_name);

        textView.setText(item.toString());

        return convertView;
    }
}
